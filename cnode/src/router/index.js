import { createRouter, createWebHashHistory } from 'vue-router'
import Home from '../views/Home.vue'

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
    redirect: '/all',
    children: [
      {
        path: '/:tab', // 动态路由 type是一个自定义的变量
        name: 'list',
        component: () => import ('../views/List')
      }
      
    ]
  },{
    path: '/detail',
    name: 'detail',
    component: () => import ('../views/Detail')
  }
]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})

export default router
