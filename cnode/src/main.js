import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
let app = createApp(App)
app.config.globalProperties.timeFormate = function (time) {
    // time 待格式化的时间
    // console.log(time);
    let newTime = new Date(time).getTime() // 待格式化时间的毫秒数
    let timeStr = new Date().getTime() // 当前时间的毫秒数
    // 用当前时间的毫秒数 减去带格式化时间的毫秒数可以获取时间差
    let str = timeStr - newTime 
    // 判断当时间差小于一分钟是 确定为刚刚发送
    let m = str/1000/60 // 时间差的分钟数
    let h = m/60 // 时间差的小时数
    let d = h/24 // 时间差的天数 
    let mt = d/30 // 时间差的月数
    let y = mt/12 // 时间差的年数
    // console.log(h);
    
    if(m < 1) { // 当分钟数小于1 
        return '刚刚'
    } else if(h < 1) { // 时间差大于一分钟但是小于一个小时
        return Math.ceil(m) + '分钟前'
    } else if(d < 1) { // 当时间差大于一个小时 小于一天
        return Math.ceil(h) + '小时前'
    } else if(mt < 1) { // 当时间差大于一天 但是小于一个月
        return Math.ceil(d) + '天前'
    } else if(mt > 1 && mt < 12) { // 当时差在一个月到12个月之间
        return Math.ceil(mt) + '月前'
    } else if(y > 1) { // 当时间差大于一年是
        return Math.ceil(y) + '年前'
    } 

}
app.use(store).use(router).mount('#app')
